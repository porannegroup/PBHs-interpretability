# PBHs prediction and Interpretability
Interpretable deep learning was used to identify structure-property relationships governing the HOMO-LUMO gap and relative stability of polybenzenoid hydrocarbons (PBHs). To this end, a ring-based graph representation was used. In addition to affording reduced training times and excellent predictive ability, this representation could be combined with a subunit-based perception of PBHs, allowing chemical insights to be presented in terms of intuitive and simple structural motifs. The resulting insights agree with conventional organic chemistry knowledge and electronic-structure based analyses, and also reveal new behaviors and identify influential structural motifs. In particular, we evaluated and compared the effects of linear, angular, and branching motifs on these two molecular properties, as well as explored the role of dispersion in mitigating torsional strain inherent in non-planar PBHs. Hence, the observed regularities and the proposed analysis  contribute to a deeper understanding of the behavior of PBHs and form the foundation for design strategies for new functional PBHs. 

<p align="center">
<img src="Interp-example.png" width="400" >
</p>

## preparations
1. Download repository  
2. Download dataset (`csv` and `xyzs`) from [COMPAS](https://gitlab.com/porannegroup/compas)
3. Update `csv` and `xyzs` paths in `utiles/args.py`
4. Install conda environment according to the instructions below

## Dependencies
```
conda create -n PBHs python=3.8
conda activate PBHs
conda install pytorch=1.10 cudatoolkit=10.2 -c pytorch
conda install -c dglteam dgl-cuda10.2
conda install numpy matplotlib networkx scipy tensorboard pandas 
pip install requests
```

## Usage
### Training
During training, the script `train.py` trains the model using the train and validation datasets. After the training is done, the script runs an evaluation on the test set and prints the results. 
The saved model and the plots of the prediction vs the target values are saved in `summary/{exp_name}`. The tensorboard log files are also saved in this directory.
```
python train.py --name 'exp_name' --target_features 'GAP_eV, Erel_eV' 
```

target_features should be separate with '`,`'.  
A full list of possible arguments can be found in `utiles/args.py`.  

### Evaluation
To only run the evaluation on a previously trained model, run the following command.
```
python eval.py --name 'exp_name'
```

### Interpretability
To run the interpretability algorithm, use `interpretability_from_names.py`. This script saves all the molecules in the dataset with their GradRAM weights in the same directory as the models and logs.
```
python interpretability_save_all.py --name 'exp_name'
```
To run only on a subset of molecules, run `interpretability_from_names.py` and change the names of the molecules in the list in line 27.

## Repo structure
- `data` -  Dataset and data related code.
- `se3_transformers` - SE3 model used as a predictor.
- `utils` - Helper functions. 
- `summary` - Logs, trained models, plots, and interpretability figures for each experiment. 


